import timed_automata_object_library as taol


def start():
    current_template = None
    while True:
        input_statements = raw_input()

        if ".xml" in input_statements:
            output_file = input_statements
            break
        input_statements = input_statements.split('.')
        for line in input_statements:
            if "can be" in line and "and it is initially" in line:
                if current_template is not None:
                    current_template.finish_template()
                    del current_template
                line = line.split()
                location_names = [line[-1]]
                n = line.index("and")
                for i in range(3, n):
                    if line[i] != location_names[0]:
                        location_names.append(line[i])
                current_template = taol.create_template(line[0], location_names)
            elif "can only be" in line:
                if current_template:
                    current_template.finish_template()
                    del current_template
                line = line.split()
                current_template = taol.create_template(line[0], [line[-1]])
            elif "It must be" in line and "in every" in line:
                line = line.split()
                location_list = []
                for i in current_template.Locations:
                    if i.Name != line[3]:
                        location_list.append(i.Name)
                current_template.create_clock(assignment_info=[line[3], None],
                                              invariant_info=[location_list, ["<=", line[-1]]])
            elif "It becomes" in line and "from all locations" in line:
                line = line.split()
                source_list = []
                for i in current_template.Locations:
                    if i.Name != line[2]:
                        source_list.append(i.Name)
                current_template.create_transition(source_list, [line[2]])
            elif "It becomes" in line and "from" in line and "and sends" in line:
                line = line.split()
                current_template.create_transition([line[4]], [line[2]], line[-1] + '!')
            elif "It becomes" in line and "from" in line:
                line = line.split()
                current_template.create_transition([line[4]], [line[2]])
            elif "It sends" in line and "while it is" in line:
                line = line.split()
                current_template.create_transition([line[-1]], [line[-1]], line[2] + '!')
            elif "The time spent while it is" in line and "cannot be more than" in line:
                line = line.split()
                current_template.create_clock(assignment_info=[None, line[6]],
                                              invariant_info=[[line[6]], ["<=", line[-1]]])
            elif ("If it receives" in line and
                  "and the time spent while it is" in line and
                  "then it becomes" in line and "from" in line):
                line = line.split()
                source = line[-1]
                target = line[-3]
                cond_num_list = []
                if "more" in line:
                    cond_num_list = [" <= ", line[16]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[16]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[15]]
                temp_transition = current_template.create_transition([source], [target], line[3] + '?')[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[None, source])
            elif ("If it receives" in line and
                  "and the time spent after leaving" in line and
                  "then it becomes" in line and "from" in line):
                line = line.split()
                source = line[-1]
                target = line[-3]
                left_location = line[10]
                cond_num_list = []
                if "more" in line:
                    cond_num_list = [" <= ", line[15]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[15]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[14]]
                temp_transition = current_template.create_transition([source], [target], line[3] + '?')[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[left_location, None])
            elif "If it receives" in line and "then it becomes" in line and "from" in line:
                line = line.split()
                source = line[9]
                target = line[7]
                current_template.create_transition([source], [target], line[3] + '?')
            elif ("If the time spent while it is" in line and
                  "then it becomes" in line and
                  "from" in line and "and sends" in line):
                line = line.split()
                source = line[-3]
                target = line[-5]
                if "more" in line:
                    cond_num_list = [" <= ", line[12]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[12]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[11]]
                temp_transition = current_template.create_transition([source], [target], line[-1] + '!')[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[None, source])
            elif ("If the time spent while it is" in line and
                  "then it becomes" in line and
                  "from" in line):
                line = line.split()
                source = line[-1]
                target = line[-3]
                if "more" in line:
                    cond_num_list = [" <= ", line[12]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[12]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[11]]
                temp_transition = current_template.create_transition([source], [target])[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[None, source])
            elif ("If the time spent after leaving" in line and
                  "then it becomes" in line and
                  "from" in line and "and sends" in line):
                line = line.split()
                source = line[-3]
                target = line[-5]
                left_location = line[6]
                cond_num_list = []
                if "more" in line:
                    cond_num_list = [" <= ", line[11]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[11]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[10]]
                temp_transition = current_template.create_transition([source], [target], line[-1] + '!')[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[left_location, None])
            elif ("If the time spent after leaving" in line and
                  "then it becomes" in line and
                  "from" in line):
                line = line.split()
                source = line[-1]
                target = line[-3]
                left_location = line[6]
                cond_num_list = []
                if "more" in line:
                    cond_num_list = [" <= ", line[11]]
                elif "less" in line:
                    cond_num_list = [" >= ", line[11]]
                elif "equal" in line:
                    cond_num_list = [" == ", line[10]]
                temp_transition = current_template.create_transition([source], [target])[0]
                current_template.create_clock(guard_info=[temp_transition, cond_num_list],
                                              assignment_info=[left_location, None])
    current_template.finish_template()
    del current_template
    taol.write_to_xml_file(output_file)


start()
