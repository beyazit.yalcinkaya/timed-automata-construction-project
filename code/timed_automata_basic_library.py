from pyuppaal import *
import sys
from sys import exit

nta = NTA()
current_template = None
template_count = 0


def get_location(loc):
    """
    Finds location object by its name.

    Args:
        loc: Location name.

    Returns:
        Location object with the given name.
    """
    global current_template, template_count, nta
    try:
        return current_template.get_location_by_name(loc)
    except IndexError:
        print "Location Does Not Exists"
        exit()


def add_current_template_to_nta():
    """
    Adds current_template the the nta object.
    """
    global current_template, template_count, nta
    if current_template:
        template_count += 1
        nta.add_template(current_template)
        nta.system += "Process" + str(template_count) + " = " + current_template.name.value + "();\n"
        current_template = None


def create_template(template_name,
                    list_of_locations):  # while proccessing input make initial location the first element
    """
    Creates a new template with given name and locations.

    Args:
        template_name: Name of the template.
        list_of_locations: List of location in the template.
    """
    global current_template, template_count, nta
    n = len(list_of_locations)
    temp = [Location(name=list_of_locations[i]) for i in range(0, n)]
    current_template = Template(Label("name", template_name), locations=temp, initlocation=temp[0])
    current_template.assign_ids()


def add_invariant(loc, var, list_of_invariants):
    """
    Adds an invariant to current_template.

    Args:
        loc: Location Name.
        var: Clock name.
        list_of_invariants: List of condition-number pairs elements.
    """
    global current_template, template_count, nta
    temp = get_location(loc)
    invariant_string = ""
    for i in list_of_invariants:
        invariant_string += var + i[0] + i[1] + " && "
        if var and " " + var + ";" not in nta.declaration:
            nta.declaration += "clock " + var + ";\n"
    invariant_string = invariant_string[:-4]
    if temp.invariant.value:
        temp.invariant.value += " && " + invariant_string
    else:
        temp.invariant.value = invariant_string


def create_transition(source, target, synch=""):
    """
    Creates a new transition from given source
    to given target with given synchronisation.

    Args:
        source: Source location.
        target: Target location.
        synch: Synchronisation name.

    Returns:
        Index o the created transition in the
        current_template's transitions list.
    """
    global current_template, template_count, nta
    if synch and " " + synch[:-1] + ";" not in nta.declaration:
        nta.declaration += "chan " + synch[:-1] + ";\n"
    current_template.transitions += [Transition(get_location(source), get_location(target), synchronisation=synch)]
    return len(current_template.transitions) - 1


def add_guard(transition_id, var, list_of_guards):
    """
    Adds a guard to the current_template.

    Args:
        transition_id: Transition id of the transition
                       on which the guard expression will
                       be inserted. It is the index of the
                       transition in the current_template's
                       transitions list.
        var: Clock name.
        list_of_guards: List of condition-number pairs elements.
    """
    guard_string = ""
    for i in list_of_guards:
        guard_string += var + i[0] + i[1] + " && "
        if var and " " + var + ";" not in nta.declaration:
            nta.declaration += "clock " + var + ";\n"
    guard_string = guard_string[:-4]
    current_template.transitions[transition_id].guard.value = guard_string


def add_assignment(transition_id, clock):
    """
    Adds a assignment to the current_template.

    Args:
        transition_id: Transition id of the transition
                       on which the assignment will
                       be inserted. It is the index of the
                       transition in the current_template's
                       transitions list.
        clock: Clock name.
    """
    assignment_string = clock + " := 0"
    if clock != "" and " " + clock + ";" not in nta.declaration:
        nta.declaration += "clock " + clock + ";\n"
    if current_template.transitions[transition_id].assignment.value != "":
        current_template.transitions[transition_id].assignment.value += ", " + assignment_string
    else:
        current_template.transitions[transition_id].assignment.value = assignment_string


def finish(line):
    """
    Finishes the model. Writes model to the xml file with gicen name.

    Args:
        line: Name of the output xml file.
    """
    global current_template, template_count, nta
    template_count += 1
    nta.system += "system Process" + ", Process".join(str(i) for i in range(1, template_count))
    nta.system += ";\n"
    map(lambda x: x.layout(), nta.templates)
    xml_file = open(line, "w")
    xml_file.write(nta.to_xml())
    xml_file.close()
