import sys
from sys import exit
import timed_automata_basic_library as tabl


def create_template(template_name, list_of_locations):
    """
    Creates a new template with the given list of locations.

    Args:
        template_name: Name of the template.
        list_of_locations: List of all locations in the template.
                           The first element is the initial location.

    Returns:
        Created template object.
    """
    tabl.create_template(template_name, list_of_locations)
    current_template = Template(name=template_name)
    for location in list_of_locations:
        current_template.add_location(Location(name=location))
    return current_template


def write_to_xml_file(output_file):
    """
    Writes created model to the given xml file.

    Args:
        output_file: Name of the xml file.
    """
    tabl.finish(output_file)


class Template(object):
    """
    This class contains all concepts of a template.

    Attributes:
        name: Name of the template.
        locations: List of all locations in the tamplate.
        transitions: List of all transitions in the tamplate.
        clocks: List of all clocks in the tamplate.
        available_clocks: List of all unused letters in alphabet.
                          (Used for picking variable names.)
    """

    def __init__(self, name=""):
        """
        Initializes Template with the given name.
        """
        self.name = name
        self.locations = []
        self.transitions = []
        self.clocks = []
        self.available_clocks = [True] * 26

    def create_transition(self, source_list, target_list, synchronisation=""):
        """
        Creates all transitions from given set of sources
        to given set of targets with given synchronisation.

        Args:
            source_list: List of locations that will be sources of the transitions.
            target_list: List of locations that will be targets of the transitions.
            synchronisation: Synchronisation on the transition.
        Returns:
            List of all created transitions.
        """
        transition_list = []
        if len(source_list) == 1 and len(target_list) == 1:
            source = self.find_location_by_name(source_list[0])
            target = self.find_location_by_name(target_list[0])
            if not source or not target:
                print "Error: Undefined Location"
                exit()
            transition_id = tabl.create_transition(source.name,
                                                    target.name,
                                                    synchronisation)
            transition_list.append(Transition(transition_id=transition_id,
                                              source=source,
                                              target=target,
                                              synchronisation=synchronisation))
            self.add_transition(transition_list[-1])
            source.add_next(target)
        elif len(source_list) > 1 and len(target_list) == 1:
            target = self.find_location_by_name(target_list[0])
            for i in source_list:
                source = self.find_location_by_name(i)
                if not source or not target:
                    print "Error: Undefined Location"
                    exit()
                transition_id = tabl.create_transition(source.name,
                                                        target.name,
                                                        synchronisation)
                transition_list.append(Transition(transition_id=transition_id,
                                                  source=source,
                                                  target=target,
                                                  synchronisation=synchronisation))
                self.add_transition(transition_list[-1])
                source.add_next(target)
        elif len(source_list) == 1 and len(target_list) > 1:
            source = self.find_location_by_name(source_list[0])
            for i in target_list:
                target = self.find_location_by_name(i)
                if not source or not target:
                    print "Error: Undefined Location"
                    exit()
                transition_id = tabl.create_transition(source.name,
                                                        target.name,
                                                        synchronisation)
                transition_list.append(Transition(transition_id=transition_id,
                                                  source=source,
                                                  target=target,
                                                  synchronisation=synchronisation))
                self.add_transition(transition_list[-1])
                source.add_next(target)
        elif len(source_list) > 1 and len(target_list) > 1:
            for i in source_list:
                source = self.find_location_by_name(i)
                for j in target_list:
                    target = self.find_location_by_name(j)
                    if not source or not target:
                        print "Error: Undefined Location"
                        exit()
                    transition_id = tabl.create_transition(source.name,
                                                            target.name,
                                                            synchronisation)
                    transition_list.append(Transition(transition_id=transition_id,
                                                      source=source,
                                                      target=target,
                                                      synchronisation=synchronisation))
                    self.add_transition(transition_list[-1])
                    source.add_next(target)
        return transition_list

    def create_clock(self,
                     guard_info=None,
                     assignment_info=None,
                     invariant_info=None):
        """
        Creates a clock that appears in given guard, assignment and invariant.

        Args:
            guard_info: A list that contains a transition list in its first index
                        and a condition-number pair list in its second index.
            assignment_info: A list that contains a location as source in its first
                             index and another location as target in its second index.
            invariant_info: A list that contains a location list in its first index
                            and a condition-number pair list in its second index.
        """
        clock = Clock(name = self.get_clock_name())
        if guard_info is not None:
            clock.add_guard(guard_info[0], guard_info[1])
            guard_info[0].add_guard(clock, guard_info[1])
        if assignment_info is not None:
            source = None
            target = None
            if assignment_info[0] is not None:
                source = self.find_location_by_name(assignment_info[0])
            if assignment_info[1] is not None:
                target = self.find_location_by_name(assignment_info[1])
            clock.add_assignment(Transition(source=source, target=target))
        if invariant_info is not None:
            for i in invariant_info[0]:
                location = self.find_location_by_name(i)
                clock.add_invariant(location, invariant_info[1])
                location.add_invariant(clock, invariant_info[1])
        self.add_clock(clock)

    def add_location(self, location_obj):
        """
        Adds a location object to the Template' locations list.

        Args:
            location_obj: Location object.
        """
        if location_obj not in self.locations:
            self.locations.append(location_obj)

    def add_transition(self, transition_obj):
        """
        Adds a transition object to the Template' transitions list.

        Args:
            transition_obj: Transition object.
        """
        if transition_obj not in self.transitions:
            self.transitions.append(transition_obj)

    def add_clock(self, clock_obj):
        """
        Adds a clock object to the Template' clocks list.

        Args:
            clock_obj: Clock object.
        """
        if clock_obj not in self.clocks:
            self.clocks.append(clock_obj)

    def process_clocks(self):
        """
        Organizes all clocks that are assigned in the transitions
        containing None sources and proper targets or vice versa.
        Note that if a transition's source is None and its target
        is a proper location, it means there are transitions from
        all locations to that target, so by this function all the
        transitions ending in the given target is added to the
        assignment list of the clock under consideration and the
        same is done for the proper source None target case.
        """
        i = 0
        while  i < len(self.clocks):
            j = 0
            clock = self.clocks[i]
            while j < len(clock.assignment_list):
                transition = clock.assignment_list[j]
                if not transition.source and transition.target:
                    found = False
                    for k in self.transitions:
                        if transition.target.name == k.target.name and k.source != k.target:
                            found = True
                            clock.add_assignment(k)
                            k.add_assignment(clock)
                    if found:
                        del transition
                        del clock.assignment_list[j]
                    else:
                        print "No proper transition have been declared for reset of the clock."
                        exit()
                elif transition.source and not transition.target:
                    found = False
                    for k in self.transitions:
                        if transition.source.name == k.source.name and k.source != k.target:
                            found = True
                            clock.add_assignment(k)
                            k.add_assignment(clock)
                    if found:
                        del transition
                        del clock.assignment_list[j]
                    else:
                        print "No proper transition have been declared for reset of the clock."
                        exit()
                elif transition.source and transition.target:
                    j += 1
                else:
                    print "Possible parsing problem of the input."
                    exit()
            i += 1

    def finish_template(self):
        """
        Adds the Template to the NTA object defined in pyuppaal.
        In this library it can be thought as it is adding the
        Template to a parenting class.
        """
        self.process_clocks()
        self.based_on_reset_maps_optimization()
        self.based_on_assignment_scopes_optimization()
        for clock_obj in self.clocks:
            for transition_obj in clock_obj.guard_dict.keys():
                tabl.add_guard(transition_obj.id,
                               clock_obj.name,
                               clock_obj.guard_dict[transition_obj])
            for location_obj in clock_obj.invariant_dict.keys():
                tabl.add_invariant(location_obj.name,
                                   clock_obj.name,
                                   clock_obj.invariant_dict[location_obj])
            for transition_obj in clock_obj.assignment_list:
                tabl.add_assignment(transition_obj.id,
                                    clock_obj.name)
        tabl.add_current_template_to_nta()
                        
    def based_on_reset_maps_optimization(self):
        """
        This function optimizes the clocks with the based on reset map approach.
        It merges the clocks which are reseted in the exact same transitions.
        """
        i = 0
        while i < len(self.clocks):
            clock1 = self.clocks[i]
            j = i + 1
            while  j < len(self.clocks):
                clock2 = self.clocks[j]
                if sorted(clock1.assignment_list) == sorted(clock2.assignment_list):
                    self.merge_clocks(clock1, clock2)#put every place where clock2 is used and delete clock2
                else:
                    j += 1
            i += 1

    def merge_clocks(self, clock1, clock2):
        """
        Merges given clocks, meaning puts clock1 to the place of clock2.

        Args:
            clock1: The clock which will be written in the place of other.
            clock2: Killed clock.
        """
        if clock1 not in self.clocks or clock2 not in self.clocks:
            return
        for transition in self.transitions:
            if clock2 in transition.guard_dict.keys():
                for guard in clock2.guard_dict[transition]:
                    transition.add_guard(clock1, guard)#guard is of the form [condition, number]
                    clock1.add_guard(transition, guard)
                del transition.guard_dict[clock2]
            if clock2 in transition.assignment_list:
                transition.assignment_list.remove(clock2)
                transition.add_assignment(clock1)
                clock1.add_assignment(transition)
        for location in self.locations:
            if clock2 in location.invariant_dict.keys():
                for invariant in clock2.invariant_dict[location]:
                    location.add_invariant(clock1, invariant)
                    clock1.add_invariant(location, invariant)
                del location.invariant_dict[clock2]
        self.clocks.remove(clock2)
        self.available_clocks[ord(clock2.name) - 97] = True
        del clock2

    @staticmethod
    def is_clock_used_between(location1, location2, x):
        """
        Checks whether the clock is used as a guard in any transition
        between given locations or as an invariant in the location1.

        Args:
            location1: Source location for the transitions to be
                       checked and the location for invariant check.
            location2: Target location for the transitions to be checked.
            x: The clock that is checked.
        """
        for transition in x.guard_dict.keys():
            if location1 == transition.source and location2 == transition.target:
                return True
        for location in x.invariant_dict.keys():
            if location1 == location:
                return True
        return False

    @staticmethod
    def is_clock_reseted_between(location1, location2, x):
        """
        Checks whether the clock is assigned to zero in any
        transition between given locations.

        Args:
            location1: Source location for the transitions to be checked.
            location2: Target location for the transitions to be checked.
            x: The clock that is checked.
        """
        for transition in x.assignment_list:
            if location1 == transition.source and location2 == transition.target:
                return True
        return False

    def dfs_in_loops(self, start, previous_node, current_node, x, first_time=True):
        """
        This function is used for DFS traversal of the model. It terminates
        with the return value indicating clocks should not be merged if the
        assignment scopes of twos clock intersect, meaning if x is encountered
        as a guard or an invariant. If after searching all pathes the condition
        is not satisfied, then it returns a value indicating the two clocks
        can be merged.

        Args:
            start: Starting location of the DFS that is carried to the deeper
                   levels with this variable.
            previous_node: It is the source of the transitions to be checked and the
                           location for the invariant check.
            current_node: It is the target of the transitions to be checked.
            x: The clock that is going to be checked if it is used as a guard or
               an invariant.
            first_time: The boolean variable that indicates if it is the first time
                        of the DFS or not.

        Returns:
            0: Do not merge clocks.
            1: Merge clocks.
        """
        if not first_time:
            if self.is_clock_used_between(previous_node, current_node, x):
                return 0
            if self.is_clock_reseted_between(previous_node, current_node, x) or current_node == start:
                return 1
        for next_node in current_node.next_list:
            temp = self.dfs_in_loops(start, current_node, next_node, x, False)
            if temp == 0:
                return 0
        return 1

    def dfs_out_of_loops(self, previous_node, current_node, x, y, is_y_reset=False, first_time=True, visited=None):
        """
        This function is used for DFS traversal of the model. It terminates
        with the return value indicating clocks cannot be merged if the
        assignment scopes of twos clock intersect, meaning if x is encountered
        as a guard or an invariant after the assignment of y. If after searching
        all pathes the condition is not satisfied, then it returns a value
        indicating the two clocks can be merged. If an already visited node is
        visited again (meaning there is a looping in the model) and y is reseted
        along the way, then the DFS continues with another DFS function, because
        it is a special case which cannot be handled with the termination
        conditions of this DFS function.

        Args:
            previous_node: It is the source of the transitions to be checked and the
                           location for the invariant check.
            current_node: It is the target of the transitions to be checked.
            x: The clock that is going to be checked if it is used as a guard or
               an invariant after the reset of y.
            y: The clock that is going to be checked if it is reseted or not.
            is_y_reset: The boolean variable that indicates the usage of y for the
                        deeper levels of the DFS.
            first_time: The boolean variable that indicates if it is the first time
                        of the DFS or not.
            visited: The dictionary for visiting records of the locations.

        Returns:
            0: Do not merge clocks.
            1: Merge clocks.
        """
        if first_time:
            visited = {}
            for location in self.locations:
                visited[location] = False
        else:
            if self.is_clock_used_between(previous_node, current_node, x) and is_y_reset:
                return 0
            if self.is_clock_reseted_between(previous_node, current_node, x):
                return 1
            if visited[current_node]:
                if is_y_reset:
                    return self.dfs_in_loops(current_node, previous_node, current_node, x)
                else:
                    return 1
            is_y_reset = self.is_clock_reseted_between(previous_node, current_node, y) if not is_y_reset else is_y_reset
        visited[current_node] = True
        for next_node in current_node.next_list:
            temp = self.dfs_out_of_loops(current_node, next_node, x, y, is_y_reset, False, visited)
            if temp == 0:
                return 0
        return 1

    def based_on_assignment_scopes_optimization(self):
        """
        This function optimizes the clocks with the based on assignment scopes approach.
        It merges two clocks if the assignment scopes of which do not intersect.
        """
        i = 0
        while i < len(self.clocks):
            x = self.clocks[i]
            j = i + 1
            while j < len(self.clocks):
                y = self.clocks[j]
                merge = True
                for t1 in x.assignment_list:
                    pre1 = t1.source
                    start1 = t1.target
                    for t2 in y.assignment_list:
                        pre2 = t2.source
                        start2 = t2.target
                        if self.dfs_out_of_loops(pre1, start1, x, y) == 0 or self.dfs_out_of_loops(pre2, start2, y, x, False) == 0:
                            merge = False
                            break   
                    if not merge:
                        break
                if merge:
                    self.merge_clocks(x, y)
                else:
                    j += 1
            i += 1

    def get_clock_name(self):
        """
        TODO(beyazit): Extend the naming style. 26 clock names might not be enough.

        Finds the next available letter in alphabet for clock name.
        
        Returns:
            Next unused letter.
        """
        for i in range(26):
            if self.available_clocks[i]:
                self.available_clocks[i] = False
                return chr(i + 97)

    def find_location_by_name(self, name):
        """
        Finds the location instance of location with given name from locations list.

        Args:
            name: Name of the desired location

        Returns:
            Location instance if found.
            None if not found.
        """
        for location in self.locations:
            if location.name == name:
                return location
        return None


class Location(object):
    """
    Location class for the model. Contains all concepts of a location.

    Attributes:
        name: Name of the location.
        invariant_dict: A dictionary that contains clocks as keys and
                        condition-number pair lists as values.
        next_list: A list that contains locations which
                   can be gone from the location object.
    """

    def __init__(self, name=""):
        """
        Initializes Location with the given name.
        """
        self.name = name
        self.invariant_dict = {}
        self.next_list = []

    def add_invariant(self, clock_obj, cond_num_list):
        """
        Adds an invariant to invariant_dict.

        Args:
            clock_obj: Clock object.
            cond_num_list: Condition-number pair list.
        """
        if clock_obj in self.invariant_dict.keys():
            if cond_num_list not in self.invariant_dict[clock_obj]:
                self.invariant_dict[clock_obj].append(cond_num_list)
        else:
            self.invariant_dict[clock_obj] = [cond_num_list]

    def add_next(self, location_obj):
        """
        Adds a location to the next_list.

        Args:
            location_obj: Location object.
        """
        if location_obj not in self.next_list:
            self.next_list.append(location_obj)


class Transition(object):
    """
    Transition class for the model. Contains all concepts of a transition.

    Attributes:
        transition_id: Unique id for the transition. This number is also
                       the index of the transition in transition list kept
                       in the timed_automata_basic_library. It helps to
                       distinguish transitions and access them in
                       constant time in timed_automata_basic_library.
        source: Source location of the transition. If source of a transition
                is None and target is a proper location object, it means
                there are transitions from all locations to the target.
        target: Target location of the transition. If target of a transition
                is None and source is a proper location object, it means
                there are transitions from the source to all locations.
        guard_dict: A dictionary that contains clocks as keys and
                    condition-number pair lists as values.
        assignment_list: A list that contains assigned clocks in that transition.
        synchronisation: Synchronisation on the transition.
    """

    def __init__(self, transition_id=-1, source=None, target=None, synchronisation=""):
        """
        Initializes Transition with the given transition_id,
        source, target, and synchronisation.
        """
        self.id = transition_id
        self.source = source
        self.target = target
        self.guard_dict = {}#[[clock object, condition, int], ...]
        self.assignment_list = []#[clock variable, ...]
        self.synchronisation = synchronisation

    def add_guard(self, clock_obj, cond_num_list):
        """
        Adds guards to guard_dict.

        Args:
            clock_obj: Clock object.
            cond_num_list: Condition-number pair list.
        """
        if clock_obj in self.guard_dict.keys():
            if cond_num_list not in self.guard_dict[clock_obj]:
                self.guard_dict[clock_obj].append(cond_num_list)
        else:
            self.guard_dict[clock_obj] = [cond_num_list]

    def add_assignment(self, clock_obj):
        """
        Adds assignments to assignment_list.

        Args:
            clock_obj: Clock object.
        """
        if clock_obj not in self.assignment_list:
            self.assignment_list.append(clock_obj)


class Clock(object):
    """
    Clock class for the model. Contains all concepts of a clock.

    Attributes:
        name: Name of the clock.
        guard_dict: A dictionary that contains transitions as keys
                    and condition-number pair lists as values.
        invariant_dict: A dictionary that contains locations as keys
                        and condition-number pair lists as values.
        assignment_list: A list that contains transitions
                         in which the clock is assigned.
    """

    def __init__(self, name=""):
        """
        Initializes Clock with the given name.
        """
        self.name = name
        self.guard_dict = {}#{transition object: [[cond, num], ...]}
        self.invariant_dict = {}#{location object: [number list to check]}
        self.assignment_list = []#[[source, target]] if source or target is not specific put False 

    def add_invariant(self, location_obj, cond_num_list):
        """
        Adds invariant to invariant_dict.

        Args:
            location_obj: Location object.
            cond_num_list: Condition-number pair list.
        """
        if location_obj in self.invariant_dict.keys():
            if cond_num_list not in self.invariant_dict[location_obj]:
                self.invariant_dict[location_obj].append(cond_num_list)
        else:
            self.invariant_dict[location_obj] = [cond_num_list]

    def add_assignment(self, transition_obj):
        """
        Adds assignment to assignment_list.

        Args:
            transition_obj: Transition object.
        """
        if transition_obj not in self.assignment_list:
            self.assignment_list.append(transition_obj)

    def add_guard(self, transition_obj, cond_num_list):
        """
        Adds guard to guard_dict.

        Args:
            transition_obj: Transition object.
            cond_num_list: Condition-number pair list.
        """
        if transition_obj in self.guard_dict.keys():
            if cond_num_list not in self.guard_dict[transition_obj]:
                self.guard_dict[transition_obj].append(cond_num_list)
        else:
            self.guard_dict[transition_obj] = [cond_num_list]
