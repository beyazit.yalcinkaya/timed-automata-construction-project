# Timed Automata Construction From Structured Descriptions And Specifications
## Abstract

Cyber-physical systems (CPSs) are everywhere, from autonomous vehicles to medical devices to smart buildings. Designing such CPSs that achieve complex tasks is a tedious and error-prone process. During the design, high-level specifications describing desired functionalities, safety measures, physical properties and restrictions, and optimality criteria have to be considered. In the era of CPSs, automating the design process is crucial. Timed automata (TA), a formalism modeling real-time systems, lie at the core of the CPS theory.
 
This project will focus on developing new methods to automatically construct a partial timed automaton from system descriptions and specifications given in structured natural language. While TA synthesis from natural language is quite an ambitious goal, in this project, it is aimed to develop a tool for generating a partial TA from a set of system description and specification templates.

## About the Tool

The current version of the developed tool is able to construct TA from the High-Level Description (HLD) Set defined below. The constructed TA is written in XML format and it can be viewed and tested by using UPPAAL. The PyUppaal module is used within the program in order to map the constructed TA to proper XML format for UPPAAL.

The program consists of three parts: input_parser, timed_automata_basic_library, and timed_automata_object_library.

### input_parser
It primitively exposes the meaning of the input. It only accepts statements from the HLD Set defined below. For each statement it calls proper functions from timed_automata_object_library.

### timed_automata_basic_library
It maps the data structured by the input_parser and timed_automata_object_library to the PyUppaal classes and generates the desired TA.

### timed_automata_object_library
It contains class definitions of the TA concepts: Template, Location, Transition, and Clock. The input_parser program uses functions and classes from this module. Only the Template class is in interaction with the input_parser and it handles all concepts needed, such as creating clocks, minimizing the number of clocks et cetera.

Two approaches are used for the minimization of the number of clocks. First one is based on the reset places of the clocks in the automaton. The second one is based on the assignment scopes of the clocks and it is a solution of a more complex graph problem.

### Based on Reset Places Approach
Clocks that always have the same values in the automaton can be reduced to a single clock.

### Assignment Scope
Assignment scope of an assignment is a subgraph of the total graph of the TA starting from the assignment under consideration ending at furthest guards or invariants and between the assignment and the ending points, there must be no other assignments of that clock.

### Based on Assignment Scopes Approach
If the assignment scopes of two clocks do not intersect at any point, then they can the reduced to a single clock.

## High-Level Description Set

### Statement:
**Template_Name** *can be* **Location_Name_1 ... Location_Name_n** *and it is initially* **Location_Name_i**.
### Interpretation of the Statement:
Declares the template named **Template_Name** and its locations named as **Location_Name_1, …, Location_Name_n**. Also, indicates the initial location **Location_Name_i**.
### Statement:
**Template_Name** *can only be* **Location_Name**.
### Interpretation of the Statement:
Declares the template named **Template_Name** and it is only location named **Location_Name**. Note that, since there is only one location, there is no need to declare the initial location.
### Statement:
*It must be* **Location_Name** *in every* **#**.
### Interpretation of the Statement:
Declares that there is a clock variable reset while leaving **Location_Name** and in every location other than **Location_Name** there are invariants checking if the clock variable is not more than the given number by **#**.
### Statement:
*It becomes* **Location_Name** *from all locations*.
### Interpretation of the Statement:
Declares that there are transitions from all locations other than **Location_Name**.
### Statement:
*It becomes* **Location_Name_j** *from* **Location_Name_i** *and sends* **Synchronization_Name**.
### Interpretation of the Statement:
Declares that there is a transition from **Location_Name_i** to **Location_Name_j** and this transition send the signal named **Synchronisation_Name**.
### Statement:
*It becomes* **Location_Name_i** *from* **Location_Name_j**.
### Interpretation of the Statement:
Declares that there is a transition from **Location_Name_i** to **Location_Name_j**.
### Statement:
*It sends* **Synchronization_Name** *while it is* **Location_Name**.
### Interpretation of the Statement:
Declares that there is a transition from **Location_Name** to itself and this transition send the signal named **Synchronisation_Name**.
### Statement:
*The time spent while it is* **Location_Name** *cannot be more than* **#**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name** and this clock variable is checked for being not more than the given number **#** in **Location_Name** as an invariant.
### Statement:
*If it receives* **Synchronization_Name** *and the time spent while it is* **Location_Name_i** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_i**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j** which depends on the signal named **Synchronisation_Name**.
### Statement:
*If it receives* **Synchronization_Name** *and the time spent after leaving* **Location_Name_k** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_k**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j** which depends on the signal named **Synchronisation_Name**.
### Statement:
*If it receives* **Synchronization_Name** *then it becomes* **Location_Name_j** *from* **Location_Name_i**.
### Interpretation of the Statement:
Declares that there a transition from **Location_Name_i** to **Location_Name_j** which depends on the signal named **Synchronisation_Name**.
### Statement:
*If the time spent while it is* **Location_Name_i** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i** *and sends* **Synchronization_Name**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_i**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j** which sends the signal named **Synchronisation_Name**.
### Statement:
*If the time spent while it is* **Location_Name_i** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_i**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j**.
### Statement:
*If the time spent after leaving* **Location_Name_k** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i** *and sends* **Synchronization_Name**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_k**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j** which sends the signal named **Synchronisation_Name**.
### Statement:
*If the time spent after leaving* **Location_Name_k** *is not more than/is not less than/is equal to* **#** *then it becomes* **Location_Name_j** *from* **Location_Name_i**.
### Interpretation of the Statement:
Declares that there is a clock variable which is reset in all transition coming to **Location_Name_k**. This clock variable is checked with the given constraint and the number as a guard condition in the transition from **Location_Name_i** to **Location_Name_j**.
